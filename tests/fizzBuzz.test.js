const request = require('supertest')
const app = require('../src/app')
const fizzBuzz = require('../src/fizzbuzz/action')

describe('Test api behavior', () => {
  test('extendedFizzBuzz with missing params', async () => {
    const response = await request(app).post('/extendedFizzBuzz').send({})
    expect(response.statusCode).toBe(400)
    expect(response.text).toBe('{"error":1,"message":"Missing parameter [string1]"}')

    const response2 = await request(app).post('/extendedFizzBuzz').send({ string1: 'xx' })
    expect(response2.statusCode).toBe(400)
    expect(response2.text).toBe('{"error":1,"message":"Missing parameter [string2]"}')

    const response3 = await request(app).post('/extendedFizzBuzz').send({ string1: 'xx', string2: 'yy', int1: 9, int2: 10 })
    expect(response3.statusCode).toBe(400)
    expect(response3.text).toBe('{"error":1,"message":"Missing parameter [limit]"}')
  })
  test('extendedFizzBuzz with wrong params', async () => {
    const response = await request(app).post('/extendedFizzBuzz').send({ string1: 'x', string2: 78, int1: 'ty', int2: 89, limit: 10 })
    expect(response.statusCode).toBe(400)
    expect(response.text).toBe('{"error":1,"message":"Parameter [int1] must be an integer"}')

    const response1 = await request(app).post('/extendedFizzBuzz').send({ string1: 'x', string2: 78, int1: false, int2: 89, limit: 10 })
    expect(response1.statusCode).toBe(400)
    expect(response1.text).toBe('{"error":1,"message":"Parameter [int1] must be an integer"}')

    const response2 = await request(app).post('/extendedFizzBuzz').send({ string1: 'x', string2: 78, int1: -40, int2: 89, limit: 10 })
    expect(response2.statusCode).toBe(400)
    expect(response2.text).toBe('{"error":1,"message":"Parameter [int1] must be equal or greater than 1"}')

    const response3 = await request(app).post('/extendedFizzBuzz').send({ string1: 'x', string2: 78, int1: 0.2, int2: 89, limit: 10 })
    expect(response3.statusCode).toBe(400)
    expect(response3.text).toBe('{"error":1,"message":"Parameter [int1] must be an integer"}')
  })
  test('extendedFizzBuzz above default limit (1000000)', async () => {
    const response = await request(app).post('/extendedFizzBuzz').send({ string1: 'fizz', string2: 'buzz', int1: 3, int2: 15, limit: 1000001 })
    expect(response.statusCode).toBe(400)
    expect(response.text).toBe('{"error":1,"message":"The limit cannot be higher than 1000000"}')
  })
  test('extendedFizzBuzz success', async () => {
    const response = await request(app).post('/extendedFizzBuzz').send({ string1: 'fizz', string2: 'buzz', int1: 3, int2: 5, limit: 15 })
    expect(response.statusCode).toBe(200)
    expect(response.text).toBe('{"fizzbuzz":"1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz"}')
  })
})

describe('Test extendedFizzBuzz logic', () => {
  test('str1: nico, str2: las, int1: 2, int2: 2, limit 10', async () => {
    expect(fizzBuzz('nico', 'las', 2, 2, 10)).toBe('1,nicolas,3,nicolas,5,nicolas,7,nicolas,9,nicolas')
  })
  test('str1: nico, str2: las, int1: 3, int2: 4, limit 12', async () => {
    expect(fizzBuzz('nico', 'las', 3, 4, 12)).toBe('1,2,nico,las,5,nico,7,las,nico,10,11,nicolas')
  })
})
