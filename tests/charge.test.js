const axios = require('axios')

const testCharge = () => {
  return new Promise((resolve, reject) => {
    const results = []
    const checkEnd = () => {
      console.log('checkend', results.length)
      if (results.length === 6) {
        resolve(results)
      }
    }
    for (let i = 0; i < 6; i++) {
      axios.post('http://localhost:8081/extendedFizzBuzz', {
        string1: 'dark',
        string2: 'vador',
        int1: 2,
        int2: 3,
        limit: 1000000
      })
        .then((r) => { results.push(1) })
        .catch(err => {
          if (err.response && err.response.data.message === 'We are sorry but all our computation slot are occupied, please retry later') {
            results.push(0)
          }
        })
        .finally(() => {
          checkEnd()
        })
    }
  })
}

describe('Test fork limit Fail if the server is not launch on http://localhost:8081', () => {
  test('launch 6 simultaneous request, 2 must fail (limit at 4)', async () => {
    let checkIfServerIsAlive = false
    try {
      await axios.get('http://localhost:8081/wrongRoute')
    } catch (err) {
      if (err.response) checkIfServerIsAlive = true
    }
    expect(checkIfServerIsAlive).toBe(true)
    const results = await testCharge()
    console.log(results)
    expect(results.filter(r => r === 0).length).toBe(2)
  })
})
