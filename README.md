# test-decembre2018

### DESCRIPTION
The original fizz-buzz consists in writing all numbers from 1 to 100, and just replacing all multiples of 3 by “fizz”, all multiples of 5 by “buzz”, and all multiples of 15 by “fizzbuzz”. The output would look like this:

> “1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,fizz,...”

This project exposes a REST API endpoint that accepts five parameters : two strings (say, string1 and string2), and three integers (say, int1, int2 and limit), and returns a **JSON**

Constraints:
- It must return a list of strings with numbers from 1 to limit, where:
- all multiples of int1 are replaced by string1,
- all multiples of int2 are replaced by string2,
- all multiples of int1 and int2 are replaced by string1string2

In this case it will be in Javascript on a nodejs server.
There is only one route:
```sh
POST url/extendedFizzBuzz { string1: 'string1', string2: 'string2', int1: 3, int2: 5, limit: 100 }
```

All responses will be in JSON, for example the response for the above request would be:
```sh
{fizzbuzz: '1,2,fizz...'}
```

For more convenience the server accepts 2 Content-Type in incoming requests:
- application/json
- application/x-www-form-urlencoded

### IMPLEMENTATION EXPLANATIONS

Really few constraints were given and in order to deliver a reliable server I had to add a few more. Indeed two problems were bound to appear if the limit was very high:
- Javascript is single thread, if the computation costs are high it might block the server for incoming requests. To prevent this, the server will compute every fizzbuzz in a forked process. To prevent forked bomb I added the environment variable **FB_MAX_FORK** which will be used to stop a request in case it will create too many simultaneous forked process.
For this project a language like GO would have been a lot more efficient
- If the limit sent is through the roof, let's say a billion, it might take too much memory for the server to complete the request. Therefore I added **FB_MAX_LIMIT** which will cause an error if the limit sent is greater than this.

If strings are too long it could create a problem. However the max POST size is 100kb by default which is  why I did not update default setting for incoming requests. See [body-parser](https://github.com/expressjs/body-parser) to modify that


### REQUIREMENTS
You have to install [nodejs > 8](https://nodejs.org/dist/v10.14.1/node-v10.14.1-x64.msi) to launch the server

### HOW TO USE
**To run it:**
```
npm install
node server.js
```
To allow more convenience in the node process management you can use [PM2](http://pm2.keymetrics.io/docs/usage/quick-start/)

**To run test suite:**
```
npm run test
```

**To generate api documentation:**
```
npm run doc
```
It creates a folder doc in which there will be an index.html.

### SETTINGS
In order to manage the server easily there is some environment variables.
For pm2 you can set it in the pm2.json file in the app root directory
Variable      | Default value | Description
------------  | ------------- | -----------
PORT          | 8081          | The port on which the server will listen
CORS          | 1             | Allow CORS requests (0 to block it)
FB_MAX_LIMIT  | 1000000       | The maximum input for the limit parameter
FB_MAX_FORK   | 4             | The maximum number of simultaneous forked process

### SCALABILITY
This server can scale horizontally, the more server you have the more fizzbuzz requests can be done.