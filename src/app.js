const express = require('express') // The node web framework
const helmet = require('helmet') // A package which helps to secure express apps
const morgan = require('morgan') // Allow us to log every request
const cors = require('cors') // Allow cross origin request ONLY IF ENVIRONMENT VARIABLE CORS IS SET TO 1
const bodyParser = require('body-parser') // Parse incoming request to put it in req.body

const app = express()
app.use(helmet())
app.use(morgan('combined'))
if (Number(process.env.CORS) === 1) {
  app.use(cors())
}
// parse application/json
app.use(bodyParser.json())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
require('./routes')(app)

module.exports = app
