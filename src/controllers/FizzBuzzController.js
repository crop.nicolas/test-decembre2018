const cp = require('child_process')
const path = require('path')
const LIMIT = process.env.FB_MAX_LIMIT || 1000000
const FORK_LIMIT = process.env.FB_MAX_FORK || 4

let currentForkedProcess = 0 // keep track of current foked process

/**
 * @function {{forkCalcul}}
 * @param {*} params Parameters parsed by the controller
 * @description Will forked the process to compute fizzbuzz outside of the main thread
 * Kill the child process in the end to be sure it doesn't run in the void
 * @returns {{Promise}} A promise
 */
const forkCalcul = (params) => {
  return new Promise((resolve, reject) => {
    currentForkedProcess++
    let isFinished = true
    const fizzBuzzWorker = cp.fork(path.join(__dirname, '../fizzbuzz/worker.js'))
    fizzBuzzWorker.on('message', function (message) {
      switch (message.event) {
        case 'result':
          isFinished = true
          resolve(message.data)
          fizzBuzzWorker.kill()
          break
      }
    })
    fizzBuzzWorker.on('exit', function (code) {
      currentForkedProcess--
      if (!isFinished) {
        console.error('fizzBuzzWorker wrong exit')
        console.error(code)
        reject(new Error(`Worker ended abnormally ${code}`))
      }
    })
    fizzBuzzWorker.send(params)
  })
}

module.exports = {
  fizzBuzz: async (req, res) => {
    const params = {
      string1: { type: String, value: null },
      string2: { type: String, value: null },
      int1: { type: Number, value: null },
      int2: { type: Number, value: null },
      limit: { type: Number, value: null }
    }
    for (let key in params) {
      if (req.body[key] === undefined) {
        return res.status(400).json({ error: 1, message: `Missing parameter [${key}]` })
      }
      params[key].value = params[key].type(req.body[key])
      if (typeof params[key].value === 'number') {
        if (!(params[key].value === req.body[key] && params[key].value % 1 === 0)) { // check if the input is really an integer
          return res.status(400).json({ error: 1, message: `Parameter [${key}] must be an integer` })
        }
        if (params[key].value < 1) {
          return res.status(400).json({ error: 1, message: `Parameter [${key}] must be equal or greater than 1` })
        }
      }
    }
    if (params.limit.value > LIMIT) {
      return res.status(400).json({ error: 1, message: `The limit cannot be higher than ${LIMIT}` })
    }
    if (currentForkedProcess === FORK_LIMIT) {
      return res.status(500).json({ error: 1, message: 'We are sorry but all our computation slot are occupied, please retry later' })
    }
    try {
      const result = await forkCalcul(params)
      res.json({ fizzbuzz: result })
    } catch (err) {
      res.status(500).json({ error: 1, message: err.message })
    }
  }
}
