/**
 * @file server.js
 * @description retrieve the express app and inject it in the http package.
 * Then it listen on the port defined in the environment (PORT)
 */
const http = require('http')
const app = require('./app')
const server = http.Server(app)
const PORT = process.env.PORT || 8081
server.listen(PORT)
console.log('server listening on ', PORT)
