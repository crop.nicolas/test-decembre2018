const FizzBuzzController = require('./controllers/FizzBuzzController')

module.exports = function (app) {
  /**
   * @api {POST} /extendedFizzBuzz
   * @apiName FizzBuzz
   * @apiVersion 1.0.0
   * @apiGroup FizzBuzz
   * @apiParam {String} string1 Mandatory string 1 (ex: fizz)
   * @apiParam {String} string2 Mandatory string 2 (ex: buzz)
   * @apiParam {Integer} int1 Mandatory int 1 (ex: 3)
   * @apiParam {Integer} int2 Mandatory int 2 (ex: 5)
   * @apiParam {Integer} limit Mandatory Limit (ex: 100)
   * @apiParamExample {json} Request-Example:
   *   {
   *      "string1": "fizz",
   *      "string2": "buzz",
   *      "int1": 3,
   *      "int2": 5,
   *      "limit": 10
   *   }
   * @apiError (400) {JSON} MissingParameter When one of the five parameters is missing
   * @apiError (400) {JSON} WrongType When the server receive something different than expected eg: (a float for an int)
   * @apiError (400) {JSON} EqualOrGreaterThan1 When int1 int2 or limit is < 1
   * @apiError (400) {JSON} LimitToHigh When limit is higher than the limit set
   * @apiError (404) {JSON} PageNotFound Route not found...
   * @apiError (500) {JSON} CannotFork If the request cause the fork process to be greater than the server limit
   * @apiError (500) {JSON} ErrorDuringComputation If an error occured in the forked process
   * @apiSuccess {JSON} A JSON containing the fizzbuzz result
   * @apiSuccessExample {JSON} Success-Response:
   *  HTTP/1.1 200 OK
   *  {
   *    "fizzbuzz": "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz"
   *  }
   */
  app.post(
    '/extendedFizzBuzz',
    FizzBuzzController.fizzBuzz
  )
  /**
   * Default 404 error
   */
  app.use((req, res) => {
    res.status(404).json({ error: 1, message: 'Page not found' })
  })
}
