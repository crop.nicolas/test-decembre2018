const fizzbuzz = require('./action')
/**
 * Send datas to fizzbuzz function and transfer it to the parent process
 */
const initialisation = function (datas) {
  process.send({
    event: 'result',
    data: fizzbuzz(
      datas.string1.value,
      datas.string2.value,
      datas.int1.value,
      datas.int2.value,
      datas.limit.value
    )
  }, function () {
    process.exit(0)
  })
}
/** Entry point in node child process */
process.on('message', initialisation)
