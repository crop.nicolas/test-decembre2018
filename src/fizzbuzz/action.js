/**
 * @function {{fizzBuzz}} the fizzbuzz implementation
 * @description
 * It is less elegant but after a small benchmark the usage of strings
 * are much more efficient than arrays
 * proof:
 * const fct1 = (max) => {
    const result = Array(max)
    for (let i = 1; i <= max; i++) {
      if (i % 3 === 0) result[i] = 'test'
      else result[i] = i
    }
    return result.join(',')
  }
  for fct1(1 000 000) it takes 1172ms
  const fct3 = (max) => {
    let result = ''
    for (let i = 1; i <= max; i++) {
      if (i % 3 === 0) result += 'test'
      else result += i
      result += ','
    }
    return result
  }
  for fct3(1 000 000) it takes 215ms
 */
module.exports = function (str1, str2, int1, int2, limit) {
  let result = ''
  let str1WithComma = str1 + ',' // Save a concatenation
  let str2WithComma = str2 + ','
  for (let i = 1; i <= limit; i++) {
    if (i % int1 === 0) {
      if (i % int2 === 0) {
        result += str1 + str2WithComma
      } else {
        result += str1WithComma
      }
    } else if (i % int2 === 0) {
      result += str2WithComma
    } else {
      result += i + ','
    }
  }
  return result.substr(0, result.length - 1)
}
